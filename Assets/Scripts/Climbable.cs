﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbable : MonoBehaviour
{
    [SerializeField] private PlayerMovement player;

    private void Start()
    {
        player = player ?? FindObjectOfType<PlayerMovement>();
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.CompareTag("Player")) {
            player.SetClimbing();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if(other.gameObject.CompareTag("Player")) {
            player.SetJumping();
        }
    }
}
