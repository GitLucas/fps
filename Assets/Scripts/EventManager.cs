﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{

    private static EventManager instance;
    
    public static EventManager Instance {
        get {
            if(instance == null)
                instance = new GameObject("EventManager").AddComponent<EventManager>();

            return instance;
        }
    }

//-----------------------------------------------------------------------------
    public event System.Action OnCoinGrabbed;
    public event System.Action OnUIUpdate;

//-----------------------------------------------------------------------------

    void Awake()
    {
        instance = this;
    }

    public void CoinGrabbed() {
        OnCoinGrabbed?.Invoke();
    }

    public void UpdateUI() {
        OnUIUpdate?.Invoke();
    }

    
}
