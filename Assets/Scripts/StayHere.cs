﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayHere : MonoBehaviour
{
    private Vector3 distance;
    private Transform player;
    private bool attached;

    private void LateUpdate() {
        if(attached) {
            player.position = transform.position + distance;
        }
    }
    
    private void OnCollisionStay(Collision other) {
        if(other.gameObject.CompareTag("Player")) {
            distance = other.transform.position - transform.position;
            attached = true;
            player = other.transform;
        }
    }

    private void OnCollisionExit(Collision other) {
        
        if(other.gameObject.CompareTag("Player")) {
            attached = false;
            player = null;
        }
    }
}
