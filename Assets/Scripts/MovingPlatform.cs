﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    [SerializeField] private float distance = 5f;
    [SerializeField] private int value = 0;
    [SerializeField] private float speed = 5f;

    private Vector3 target, left, right;

    // Start is called before the first frame update
    void Start()
    {
        left = right = transform.position;
        
        if(value == 0) {
            left.x  -= distance;
            right.x += distance;
        }
        else if (value == 1) {
            left.y  -= distance;
            right.y += distance;
        } else {
            left.z  -= distance;
            right.z += distance;
        }

        target = left;

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);

        if(Vector3.Distance(transform.position, target) < 0.005f) 
            target = (target == left) ? right : left;
    }
}
