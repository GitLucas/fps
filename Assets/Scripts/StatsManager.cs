﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{

    private static StatsManager instance;
    
    public static StatsManager Instance {
        get {
            if(instance == null)
                instance = new GameObject("StatsManager").AddComponent<StatsManager>();

            return instance;
        }
    }
//----------------------------------------------------------------------------

    private int coins;

//----------------------------------------------------------------------------

    void Awake()
    {
        instance = this;
    }

    void Start() {
        EventManager.Instance.OnCoinGrabbed += HandleCoinGrabbed;
    }

    void HandleCoinGrabbed() {
        coins++;
        Debug.Log("coins: " + coins);
    }
}
