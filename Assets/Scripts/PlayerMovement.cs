﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private Transform head, ghost;
    [SerializeField] private float speed = 1f, jumpForce = 5f;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private Collider myCollider;
    [SerializeField] private Animator myAnimator;

    private bool canDash = true;
    private State state;
    Quaternion auxQ;

    float defaultSpeed;

    private void Start()
    {
        defaultSpeed = speed;
    }
    
    void Update()
    {

        //x = Input.GetAxis("Horizontal");
        //y = Input.GetAxis("Vertical");

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        auxQ = head.rotation;
        auxQ.x = 0f;
        auxQ.z = 0f;
        ghost.rotation = auxQ;

        Vector3 tempVect = ghost.forward * v + ghost.right * h;
        
        speed = (Input.GetKey(KeyCode.LeftShift)) ? defaultSpeed * 2 : defaultSpeed;

        tempVect = tempVect.normalized * speed * Time.deltaTime;
        rigidbody.MovePosition(transform.position + tempVect);

        myAnimator.SetBool("Walking", h != 0 || v != 0);


        if(CanJump()) {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            HandleJump();
        }

        if(Input.GetMouseButtonDown(0) && canDash) {
            rigidbody.AddForce(ghost.forward * jumpForce * 2, ForceMode.Impulse);
            canDash = false;
        }

        /*Debug.DrawRay(transform.position + Vector3.right * .3f,   Vector3.down , Color.green);
        Debug.DrawRay(transform.position + Vector3.left * .3f,    Vector3.down , Color.green);
        Debug.DrawRay(transform.position + Vector3.forward * .3f, Vector3.down , Color.green);
        Debug.DrawRay(transform.position + Vector3.back * .3f,    Vector3.down , Color.green);*/
    }

    bool CanJump() {
        return Input.GetButtonDown("Jump") && state != State.DoubleJumping;
    }

    private void OnCollisionEnter(Collision other) {
        if (Physics.Raycast(transform.position + Vector3.right * .3f    , -transform.up, 1f) || 
            Physics.Raycast(transform.position + Vector3.left * .3f     , -transform.up, 1f) ||
            Physics.Raycast(transform.position + Vector3.forward * .3f  , -transform.up, 1f) ||
            Physics.Raycast(transform.position + Vector3.back * .3f     , -transform.up, 1f)) {
            state = State.Grounded;
            canDash = true;
        } 
       
    }

    void HandleJump() {
        switch(state) {
            case State.Grounded:
                state = State.Jumping;
                break;
            case State.Jumping:
                state = State.DoubleJumping;
                break;
        }
    }

    /**
    * For external use only
    */
    public void SetClimbing() {
        state = State.Climbing;
    }

    /**
    * For external use only
    */
    public void SetJumping() {
        if(state != State.Grounded) {
            state = State.Jumping;
            canDash = true;
        }
    }

    public enum State {
        Grounded,
        Climbing,
        Jumping,
        DoubleJumping
    }
}
