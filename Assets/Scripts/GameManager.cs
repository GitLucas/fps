﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    
    public static GameManager Instance {
        get {
            if(instance == null)
                instance = new GameObject("GameManager").AddComponent<GameManager>();

            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}
