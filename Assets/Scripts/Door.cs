﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    [SerializeField] private float speed = 1;
    [SerializeField] private Transform hinge;

    private bool inMovement;

    private void OnTriggerStay(Collider other) {
        if(other.CompareTag("Player")) {
            if(Input.GetKeyDown(KeyCode.E) && !inMovement)
                Toggle();
        }
    }

    private void Toggle() {
        inMovement = true;
        StartCoroutine(ToggleCoroutine());
    }

    private IEnumerator ToggleCoroutine() {

        int sign = (hinge.rotation.w == 1) ? -1 : 1;
        int i = 0;
        float repetitions = 90 / speed;

        while ( i < repetitions ) {
            hinge.Rotate(0, sign * speed, 0);
            i++;
            yield return null;
        }

        inMovement = false;

        
    }
}
